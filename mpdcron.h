#ifndef __MPDCRON_H__
#define __MPDCRON_H__
#include <mpd/client.h>
#include <mpd/async.h>

enum private_event {
	CONNECTED       = 0x1,
	DISCONNECTED    = 0x2,
    SONG_ID         = 0x4,
    PLAYER_STATE    = 0x8,
    OPTIONS_REPEAT   = 0x10,
    OPTIONS_RANDOM   = 0x20,
    OPTIONS_CONSUME  = 0x40,
    OPTIONS_SINGLE   = 0x80,

};
struct event_entry { 
    enum mpd_idle event;
	enum private_event pevent;
    char *command;
};


void do_command(struct event_entry *entry);
#endif
