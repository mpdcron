/* MPDCron 
 * Copyright (C) 2009-2009 Qball Cow <qball@sarine.nl>
 * Project homepage: http://mpd.wikia.com/
 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include <signal.h>
#include <errno.h>
#include <config.h>
#include "mpdcron.h"

#define MAX_LINE_LENGTH 1024*4

static void event_entry_free(struct event_entry *entry)
{
	if(entry == NULL) return;
	if(entry->command) free(entry->command);
	free(entry);
}

unsigned int  num_entries = 0;
struct event_entry **entries = NULL;
struct mpd_connection *conn = NULL;

/* Code borrowed from mpdscribble */
static bool quit = false;
static void signal_handler(int signum)
{
	struct mpd_connection *conn2 = conn;
	printf("Got signal: %i\n", signum);
    conn = NULL;
	quit = true;
	if(conn2)
			mpd_connection_free(conn2);
	return;
}

static void
x_sigaction(int signum, const struct sigaction *act)
{
	if (sigaction(signum, act, NULL) < 0) {
		perror("sigaction()");
		exit(EXIT_FAILURE);
	}
}

static void
setup_signals(void)
{
	struct sigaction sa;

	sigemptyset(&sa.sa_mask);
	sa.sa_flags = 0;
	sa.sa_handler = signal_handler;
	x_sigaction(SIGINT, &sa);
	x_sigaction(SIGTERM, &sa);
	x_sigaction(SIGHUP, &sa);
	sa.sa_handler = SIG_IGN;
	x_sigaction(SIGPIPE, &sa);
}

/* Parse the config file, return all the events to watch */
static enum mpd_idle parse_cronfile(FILE *fp)
{
    enum mpd_idle values_events = 0;
    char buffer[MAX_LINE_LENGTH*4];

    while(fgets(buffer,MAX_LINE_LENGTH*4, fp) != NULL)
    {
        unsigned int i = 0;
		/* comment line */
		if(buffer[0] == '#') continue; 
		/* Get the first space/tab, where the line is split. */
        for(i =0; buffer[i] != '\0' && buffer[i] != ' ' && buffer[i] != '\t';  i++);
        if((buffer[i] == ' ' || buffer[i] == '\t') && (i+2) < strlen(buffer))
        {
            enum mpd_idle event=0;
			enum private_event pevent =0;
            char *item;
            char *saveptr;
			buffer[i] =  '\0';
            item = strtok_r(&buffer[0], "|", &saveptr);
            if(item)do{
                if(strncmp(item, "PLAYER",strlen("PLAYER")) == 0) 
						event |= MPD_IDLE_PLAYER; 
				else if(strncmp(item, "DATABASE",strlen("DATABASE")) == 0) 
						event |= MPD_IDLE_DATABASE; 
				else if(strncmp(item, "STORED_PLAYLIST",strlen("STORED_PLAYLIST")) == 0) 
						event |= MPD_IDLE_STORED_PLAYLIST; 
				else if(strncmp(item, "QUEUE",strlen("QUEUE")) == 0) 
						event |= MPD_IDLE_QUEUE; 
				else if(strncmp(item, "MIXER",strlen("MIXER")) == 0) 
						event |= MPD_IDLE_MIXER; 
				else if(strncmp(item, "OUTPUT",strlen("OUTPUT")) == 0) 
						event |= MPD_IDLE_OUTPUT; 
				else if(strncmp(item, "OPTIONS",strlen("OPTIONS")) == 0) 
						event |= MPD_IDLE_OPTIONS; 
				else if(strncmp(item, "UPDATE",strlen("UPDATE")) == 0) 
						event |= MPD_IDLE_UPDATE; 
				else if (strncmp(item, "CONNECTED", strlen("CONNECTED")) == 0)
						pevent |= CONNECTED;
				else if (strncmp(item, "DISCONNECTED", strlen("DISCONNECTED")) == 0)
						pevent |= DISCONNECTED;
				else if (strncmp(item, "SONGID", strlen("SONGID")) == 0)
						pevent |= SONG_ID;
				else if (strncmp(item, "PSTATE", strlen("PSTATE")) == 0)
						pevent |= PLAYER_STATE;
				else if (strncmp(item, "REPEAT", strlen("REPEAT")) == 0)
						pevent |= OPTIONS_REPEAT;
				else if (strncmp(item, "RANDOM", strlen("RANDOM")) == 0)
						pevent |= OPTIONS_RANDOM;
				else if (strncmp(item, "CONSUME", strlen("CONSUME")) == 0)
						pevent |= OPTIONS_CONSUME;
				else if (strncmp(item, "SINGLE", strlen("SINGLE")) == 0)
						pevent |= OPTIONS_SINGLE;
				else {
					fprintf(stderr, "Unknown event found: %s\n", item);
					exit(EXIT_FAILURE);
				}
                item = strtok_r(NULL, "|",&saveptr);
            }while(item != NULL);
            num_entries++;
            entries = realloc(entries, (size_t)(num_entries+1)*sizeof(*entries));
            entries[num_entries-1] = malloc(sizeof(struct event_entry));
            entries[num_entries-1]->event = event;
			entries[num_entries-1]->pevent = pevent;
			entries[num_entries-1]->command = strdup(&buffer[i+1]);
            entries[num_entries] = NULL;

            if((pevent&(SONG_ID|PLAYER_STATE))>0) event |= MPD_IDLE_PLAYER;
            if((pevent&(OPTIONS_SINGLE|OPTIONS_REPEAT|OPTIONS_RANDOM|OPTIONS_CONSUME))>0) event |= MPD_IDLE_OPTIONS;
            values_events |= event;
        }
    }
    return values_events;
}
#define SIGNAL_PEVENT(a) for(unsigned int i=0; i< num_entries; i++){\
			if((a&(entries[i]->pevent)) != 0) {\
					do_command(entries[i]);\
			}\
	}

#define SIGNAL_EVENT(a) for(unsigned int i=0; i< num_entries; i++){\
			if((a&(entries[i]->event)) != 0) {\
					do_command(entries[i]);\
			}\
	}

int main (int argc, char **argv)
{
    bool daemonize = true;
    bool file_specified = false;
    const char *hostname = NULL;
    int port = 0;

    struct mpd_status *status = NULL;
    enum mpd_idle listen_to_events=0;

    /**
     * Parse the commandline options 
     */
    if(argc >= 2)
    {
        for(unsigned int i=1;i<(unsigned int)argc;i++)
        {
            if(argv[i][0] == '-'){
                if(strcmp(argv[i], "-f") ==0) daemonize = false;
                else if (strncmp(argv[i], "--hostname=", 11) == 0){
                    hostname = &(argv[i][11]);
                } else if (strncmp(argv[i], "--port=", 7) == 0){
                    port = atoi(&(argv[i][7]));
                } else if (strcmp(argv[i], "-v") == 0 || strcmp(argv[i], "--version") == 0) {
                    printf("%s\n", PACKAGE_STRING);
                    exit(EXIT_SUCCESS);
                } else if (strcmp(argv[i], "-h") == 0 || strcmp(argv[i], "--help") == 0) {
                    printf("%s <OPTIONS> <CONFIG FILE> ...\n\n", PACKAGE_STRING);
                    printf("-f         			            Do not daemonize\n");
                    printf("--hostname=<password>@<name>    Set hostname\n");
                    printf("--port=port             		Set port\n");
                    printf("--help -h               		Help message\n");
                    printf("--version -v					Print version\n");
                    printf("\n");
                    exit(EXIT_SUCCESS);
                } else {
                    fprintf(stderr, "Unknown parameter: %s\n", argv[i]);
                    exit (EXIT_FAILURE);
                }
            } else {
                FILE *fp = NULL;
                /* Open the file */ 
                fp = fopen(argv[i], "r");
                if(!fp) {
                    fprintf(stderr, "Failed to open file: %s: %s\n", argv[i], strerror(errno));
                    exit(EXIT_FAILURE);
                }
                /* Parse file */
                listen_to_events |= parse_cronfile(fp);
                fclose(fp);
                file_specified = true;
            }
        }
    }
    /**
     * If no file specified try ~/.mpdcron
     */
    if(!file_specified)
    {
        FILE *fp = NULL;
        char buffer[MAX_LINE_LENGTH];
        const char *homedir = getenv("HOME");
        if(homedir == NULL) {
            fprintf(stderr, "Homedir not found\n");
            exit(EXIT_FAILURE);
        }
        /* Create path to config file */
        snprintf(buffer,MAX_LINE_LENGTH, "%s/.mpdcron", homedir);
        /* Open the file */ 
        fp = fopen(buffer, "r");
        if(!fp) {
            /* not found is fatal here */
            fprintf(stderr, "No mpdcron file found\n");
            exit(EXIT_FAILURE);
        }
        /* Parse file */
        listen_to_events |= parse_cronfile(fp);
        fclose(fp);
    }

    /* Check if there is atleast one event configured */
    if(num_entries == 0){
        fprintf(stderr, "No entry specified. Nothing to do.\nQuitting");
        return EXIT_FAILURE;
    }

    /* Setup the signal handling */
    setup_signals();
    /* Daemonize */
    if(daemonize) {
        switch (fork()){
            case 0:
                break;	
            case -1:
            default:
                exit(EXIT_SUCCESS);
        }
    }

    /* Try to connect to mpd.  */
    do{
        conn = mpd_connection_new(hostname, port, 5000);
        if(conn == NULL) {
            fprintf(stderr, "Failed to allocate memory\n");
            return EXIT_FAILURE;
        }

        if (mpd_connection_get_error(conn) != MPD_ERROR_SUCCESS) {
            fprintf(stderr,"%s\n", mpd_connection_get_error_message(conn));
            mpd_connection_free(conn);
            conn = NULL;
        }else{
            SIGNAL_PEVENT(CONNECTED);
            /* If connect success break out of while loop */ 
            break;
        }
        /* sleep and sighandler does not work, fix this */
        sleep(5);
    }while(!quit);

    if(!quit){
            status = mpd_run_status(conn);
    }
    /* loop */
    while(!quit)
    {
        /* Block until mpd send command */
        enum mpd_idle idle_event = mpd_run_idle_mask(conn,listen_to_events|MPD_IDLE_PLAYER);
        /* Called to quit */
        if(quit) break;

        if(conn && (idle_event) > 0 ) {
            struct mpd_status *st2 = mpd_run_status(conn);
            enum private_event pevent = 0;
            if(status)
            {
                if(mpd_status_get_song_id(status) != mpd_status_get_song_id(st2))
                    pevent |=  SONG_ID;
                if(mpd_status_get_state(status) != mpd_status_get_state(st2))
                    pevent |=  PLAYER_STATE;
                if(mpd_status_get_repeat(status) != mpd_status_get_repeat(st2))
                    pevent |=  OPTIONS_REPEAT;
                if(mpd_status_get_random(status) != mpd_status_get_random(st2))
                    pevent |=  OPTIONS_RANDOM;
                if(mpd_status_get_consume(status) != mpd_status_get_consume(st2))
                    pevent |=  OPTIONS_CONSUME;
                if(mpd_status_get_single(status) != mpd_status_get_single(st2))
                    pevent |=  OPTIONS_SINGLE;
                mpd_status_free(status);
            }
            if(pevent) {
                SIGNAL_PEVENT(pevent);
            }
            status = st2;
        }
        SIGNAL_EVENT(idle_event);

        /* Idle event was 0, this mean something went wrong.. */
        if(idle_event == 0) {
            enum mpd_error error = mpd_connection_get_error(conn);
            /* If the connection gave a timeout or an explicit close, try to reconnect */
            if(conn && (error&(MPD_ERROR_TIMEOUT|MPD_ERROR_CLOSED))) {
                /* We got disconnected somehow. Lets try to reconnect */
                mpd_connection_free(conn);
                conn = NULL;
                SIGNAL_PEVENT(DISCONNECTED);
                do{
                    /* Reconnect */
                    fprintf(stdout,"reconnect\n");
                    conn = mpd_connection_new(hostname, port, 5000);
                    /* Check success */ 
                    if (mpd_connection_get_error(conn) != MPD_ERROR_SUCCESS) {
                        fprintf(stderr,"%s\n", mpd_connection_get_error_message(conn));
                        mpd_connection_free(conn);
                        conn = NULL;
                        sleep(5);
                    }
                    else
                        break;
                    /* Retry util success */
                }while(!quit);	

                SIGNAL_PEVENT(CONNECTED);

                /* Continue with the while loop */
                continue;
            }
            /* Else stop loop */
            break;
        }
    }
    printf("quitting\n");
    if(status)
            mpd_status_free(status);
    /* Close/cleanup connection */
    if(conn)
        mpd_connection_free(conn);
    /* Free the entries */
    for(unsigned int i=0;i<num_entries;i++) 
        event_entry_free(entries[i]);
    free(entries);
}
